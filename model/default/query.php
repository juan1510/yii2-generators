<?php

/**
 * Esta es la plantilla para generar la clase ActiveQuery.
 */

/* @var $this yii\web\View */
/* @var $generator app\generators\model\Generator */
/* @var $className string Nombre de la Clas */
/* @var $modelClassName string  Nombre de la clase Modelo */

$modelFullClassName = $modelClassName;
if ($generator->ns !== $generator->queryNs) {
    $modelFullClassName = '\\' . $generator->ns . '\\' . $modelFullClassName;
}

echo "<?php\n";
?>

namespace <?= $generator->queryNs ?>;
use app\helpers\Strings;

/**
 * Esta es la clase ActiveQuery para [[<?= $modelFullClassName ?>]].
 *
 * @package app
 * @subpackage models/base
 * @category Models
 *
 * @author <?= Yii::$app->params['developers'][$generator->developer] ?> <<?= $generator->developer ?>>
 * @copyright Copyright (c) <?= date('Y') . " {$generator->copyright} \n" ?>
 * @version 0.0.1
 * @since <?= $generator->since . "\n" ?>
 *
 * @see <?= $modelFullClassName . "\n" ?>
 */
class <?= $className ?> extends <?= '\\' . ltrim($generator->queryBaseClass, '\\') . "\n" ?>
{
    /*public function active()
    {
        $this->andWhere('[[activo]] = '.self::STATUS_ACTIVE);
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return <?= $modelFullClassName ?>[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return <?= $modelFullClassName ?>|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
