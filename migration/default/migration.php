<?php

/**
 * This is the template for generating the model class of a specified table.
 */

/* @var $this yii\web\View */
/* @var $generator mootensai\enhancedgii\migration\Generator */
/* @var $migrationName string migration name */

echo "<?php

/**
 * Ésta es la migración para la tabla '{$tableName}'
 *
 * @package app
 * @subpackage migrations
 * @category Migrations
 *
 * @author " . Yii::$app->params['developers'][$generator->developer] . " <{$generator->developer}>
 * @copyright Copyright (c) " . date('Y') . " {$generator->copyright}
 * @version 0.0.1
 * @since {$generator->since}
 */
class {$migrationName} extends \yii\db\Migration
{
    /**
     *  @var string
     *  Nombre de la tabla en la BD
     */
    public \$tableName = '{$tableName}';

    /**
     * @inheritdoc
     */
";

if ($generator->isSafeUpDown) :
    echo "    public function safeUp()";
else :
    echo "    public function up()";
endif;
echo "{\n";
if ($generator->createTableIfNotExists) :
    echo "        \$tables = Yii::\$app->db->schema->getTableNames();";
endif;

echo "        \$tableOptions = null;

        if (\$this->db->driverName === 'mysql') {
            \$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }\n";
?>

<?php foreach ($tables as $table) :

    $tableRaw = trim($table['name'], '{}%');
    $t = '';
    if ($generator->createTableIfNotExists == 1) : $t = '  ';
?>
        if (!in_array(Yii::$app->db->tablePrefix.$this->tableName, $tables)) {
    <?php endif; ?>
    <?= $t ?>
    $this->createTable($this->tableName, [
    <?php foreach ($table['columns'] as $column => $definition) : ?>
        <?= $t ?>
        <?= "'$column' => $definition" ?>,
    <?php endforeach; ?>
    <?php if (isset($table['primary'])) : ?>
        <?= $t ?>
        <?= "'{$table['primary']}'" ?>,
    <?php endif; ?>
    <?= $t ?>], $tableOptions);

    $this->addCommentOnTable($this->tableName,'<?= $table['comment'] ?>');

    // Indexes
    <?php foreach ($table['indexes'] as $index) : ?>
        $this->execute('<?= $index ?>');
    <?php endforeach; ?>

    // Relations
    <?php foreach ($table['relations'] as $relation) : ?>
        $this->execute('<?= $relation ?>');
    <?php endforeach; ?>

    // Constraints
    <?php foreach ($table['checks'] as $check) : ?>
        $this->execute("ALTER TABLE {$this->tableName} ADD CONSTRAINT \"<?= $check->name ?>\"
        <?= $check->expression ?>");
    <?php endforeach; ?>

    <?php foreach ($table['tableSchema']->columns as $column => $columnData) : ?>
        $this->addCommentOnColumn($this->tableName, '<?= $column ?>', "<?= $columnData->comment ?>");
    <?php endforeach; ?>

    <?php if ($generator->createTableIfNotExists == 1) : ?>
        } else {
        echo "\nTable `".Yii::$app->db->tablePrefix."<?= $tableRaw ?>` already exists!\n";
        }
    <?php endif; ?>
<?php endforeach; ?>

<?= "
    }

    /**
     * @inheritdoc
     */
" ?>
<?php if ($generator->isSafeUpDown) : ?>
    public function safeDown()
<?php else : ?>
    public function down()
<?php endif; ?>
<?= "   {
    " ?>
<?php if ($generator->disableFkc) : ?>
    $this->execute('SET foreign_key_checks = 0');
<?php endif; ?>
<?php foreach (array_reverse($tables) as $table) : ?>
    $this->dropTable($this->tableName);
<?php endforeach; ?>
<?php if ($generator->disableFkc) : ?>
    $this->execute('SET foreign_key_checks = 1');
<?php endif; ?>
<?= "
    }
}" ?>
