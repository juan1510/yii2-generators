<?php

use yii\helpers\StringHelper;


/* @var $this yii\web\View */
/* @var $generator magisterapp\generators\rest\Generator */

$modelClass = StringHelper::basename($generator->modelClass);
$searchModelClass = StringHelper::basename($generator->searchModelClass);
if ($modelClass === $searchModelClass) {
    $modelAlias = $modelClass . 'Model';
}
$rules = $generator->generateSearchRules();
$labels = $generator->generateSearchLabels();
$searchAttributes = $generator->getSearchAttributes();
$searchConditions = $generator->generateSearchConditions();

echo "<?php\n";
?>

namespace <?= StringHelper::dirname(ltrim($generator->searchModelClass, '\\')) ?>;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use <?= ltrim($generator->modelClass, '\\') . (isset($modelAlias) ? " as $modelAlias" : "") ?>;

/**
 * <?= $searchModelClass ?> represetna el modelo search para `<?= $generator->modelClass ?>`.
 *
 * @package app/modules
 * @subpackage api/models
 * @category Models
 *
 * @author <?= Yii::$app->params['developers'][$generator->developer] ?> <<?= $generator->developer ?>>
 * @copyright Copyright (c) <?= date('Y') . " {$generator->copyright} \n" ?>
 * @version 0.0.1
 * @since <?= $generator->since . "\n" ?>
 */
class <?= $searchModelClass ?> extends <?= isset($modelAlias) ? $modelAlias : $modelClass ?>

{

    use \magisterapp\rest\traits\SyncSearch;

    /**
     * @var integer
     * Permite establecer el número de registros a ser mostrados por pagina
     */
    public $perPage = 20;

    /**
     * @var array
     * Permite identificar los registros excluidos de la consulta
     */
    public $notIn = [];

    /**
     * @var string
     * Permite definir la ultima fecha de sincronización exitosa
     */
    public $ultimaFecha;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            <?= implode(",\n            ", $rules) ?>,
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        if (isset($params['per-page']))
        {
            $this->perPage = $params['per-page'];
        }

        if (isset($params['notIn']))
        {
            $params['notIn'] = \yii\helpers\Json::decode($params['notIn']);
            $this->notIn     = is_array($params['notIn']) ? array_values($params['notIn']) : [
                    ];
        }

        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => $this->perPage,
            ],
            /*'sort' => [
                'defaultOrder' => [
                    'nombre' => SORT_ASC,
                ]
            ],*/
        ]);

        $this->setAttributes($params);

        if (!empty($this->ultimaFecha))
        {
            $this->ultimaFecha = str_replace('_', ' ', $this->ultimaFecha);
            $query->andFilterWhere(['>', self::CREATED_DATE_COLUMN, $this->ultimaFecha]);
        }

        if ($this->perPage == 0)
        {
            $dataProvider->pagination = false;
        }

        $query->andFilterWhere(['not in', '<?= $generator->getPrimaryKey() ?>', $this->notIn]);

        <?= implode("\n        ", $searchConditions) ?>

        return $dataProvider;
    }

    /**
     * Campos disponibles en el servicio
     * @return array
     */
    public function fields()
    {
        return [
            <?php
            foreach ($generator->getColumnNames() as $column) {
                if ($column == 'activo') {
                    break;
                }
                echo "'{$column}',\n            ";
            }
            echo "\n";
            ?>
        ];
    }
}
