<?php

use yii\helpers\Inflector;

$moduleId = Inflector::camel2id($generator->module);

$items = [];
foreach ($generator->models as $model)
{
    $modelId = Inflector::camel2id($model);
    $body    = '{"field":2}';

    $services = [
        [
            'name'    => 'List',
            "request" => [
                "auth"        => [
                    "type"   => "bearer",
                    "bearer" => [
                        [
                            "key"   => "token",
                            "value" => $generator->token,
                            "type"  => "string"
                        ]
                    ]
                ],
                "method"      => "GET",
                "header"      => [],
                "body"        => [],
                "url"         => [
                    "raw"  => "{{host}}/{$moduleId}/{$modelId}",
                    "host" => [
                        "{{host}}"
                    ],
                    "path" => [
                        "{$moduleId}",
                        "{$modelId}"
                    ]
                ],
                "description" => ""
            ],
        ],
        [
            "name"     => "Create",
            "request"  => [
                "auth"        => [
                    "type"   => "bearer",
                    "bearer" => [
                        [
                            "key"   => "token",
                            "value" => $generator->token,
                            "type"  => "string"
                        ]
                    ]
                ],
                "method"      => "POST",
                "header"      => [
                    [
                        "key"   => "Content-Type",
                        "value" => "application/json"
                    ]
                ],
                "body"        => [
                    "mode" => "raw",
                    "raw"  => $body
                ],
                "url"         => [
                    "raw"  => "{{host}}/{$moduleId}/{$modelId}",
                    "host" => [
                        "{{host}}"
                    ],
                    "path" => [
                        "{$moduleId}",
                        "{$modelId}"
                    ]
                ],
                "description" => ""
            ],
            "response" => []
        ],
        [
            "name"     => "Update",
            "request"  => [
                "auth"        => [
                    "type"   => "bearer",
                    "bearer" => [
                        [
                            "key"   => "token",
                            "value" => $generator->token,
                            "type"  => "string"
                        ]
                    ]
                ],
                "method"      => "PUT",
                "header"      => [
                    [
                        "key"   => "Content-Type",
                        "value" => "application/json"
                    ]
                ],
                "body"        => [
                    "mode" => "raw",
                    "raw"  => $body
                ],
                "url"         => [
                    "raw"  => "{{host}}/{$moduleId}/{$modelId}",
                    "host" => [
                        "{{host}}"
                    ],
                    "path" => [
                        "{$moduleId}",
                        "{$modelId}"
                    ]
                ],
                "description" => ""
            ],
            "response" => []
        ],
        [
            "name"     => "Delete",
            "request"  => [
                "auth"        => [
                    "type"   => "bearer",
                    "bearer" => [
                        [
                            "key"   => "token",
                            "value" => $generator->token,
                            "type"  => "string"
                        ]
                    ]
                ],
                "method"      => "DELETE",
                "header"      => [
                    [
                        "key"   => "Content-Type",
                        "value" => "application/json"
                    ]
                ],
                "url"         => [
                    "raw"  => "{{host}}/{$moduleId}/{$modelId}/1",
                    "host" => [
                        "{{host}}"
                    ],
                    "path" => [
                        "{$moduleId}",
                        "{$modelId}"
                    ]
                ],
                "description" => ""
            ],
            "response" => []
        ],
        [
            "name"     => "Sync",
            "request"  => [
                "auth"        => [
                    "type"   => "bearer",
                    "bearer" => [
                        [
                            "key"   => "token",
                            "value" => $generator->token,
                            "type"  => "string"
                        ]
                    ]
                ],
                "method"      => "GET",
                "header"      => [
                    [
                        "key"   => "Content-Type",
                        "value" => "application/json"
                    ]
                ],
                "url"         => [
                    "raw"  => "{{host}}/{$moduleId}/{$modelId}/sync?ultimaFecha=".date('Y-m-d_H:i:s'),
                    "host" => [
                        "{{host}}"
                    ],
                    "path" => [
                        "{$moduleId}",
                        "{$modelId}",
                        "sync"
                    ]
                ],
                "description" => ""
            ],
            "response" => []
        ],
        [
            "name"     => "Create multiple",
            "request"  => [
                "auth"        => [
                    "type"   => "bearer",
                    "bearer" => [
                        [
                            "key"   => "token",
                            "value" => $generator->token,
                            "type"  => "string"
                        ]
                    ]
                ],
                "method"      => "POST",
                "header"      => [
                    [
                        "key"   => "Content-Type",
                        "value" => "application/json"
                    ]
                ],
                "body"        => [
                    "mode" => "raw",
                    "raw"  => '{"data":['.$body.'], "skipErrors":false}'
                ],
                "url"         => [
                    "raw"  => "{{host}}/{$moduleId}/{$modelId}/create-all",
                    "host" => [
                        "{{host}}"
                    ],
                    "path" => [
                        "{$moduleId}",
                        "{$modelId}",
                        "create-all"
                    ]
                ],
                "description" => ""
            ],
            "response" => []
        ]
    ];

    $items[] = [
        "name"        => Inflector::camel2words($model),
        "description" => "",
        "item"       => $services,
        "_postman_isSubFolder"=> true
    ];
}

echo json_encode([
    "info" => [
        'name'        => Inflector::id2camel($moduleId),
        "_postman_id" => "",
        "description" => "",
        "schema"      => "https://schema.getpostman.com/json/collection/v2.1.0/collection.json"
    ],
    "item" => [[
        'name'        => Inflector::id2camel($moduleId),
        "description" => "",
        'item'        => $items
    ]]
        ], JSON_PRETTY_PRINT);
?>
